import { TestBed } from '@angular/core/testing';

import { ServiceModule } from './service.module';

describe('ServiceModule', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceModule = TestBed.get(ServiceModule);
    expect(service).toBeTruthy();
  });
});
