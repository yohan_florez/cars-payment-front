import { Component, OnInit } from '@angular/core';
import { ServiceModule } from 'src/app/services/service.module';
import { UtilService } from 'src/app/services/util.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss']
})
export class ListProductsComponent implements OnInit {

  products:any = [];
  search = '*';
  categories:any;
  listProduct:any = [];

  constructor(private service: ServiceModule, private utilService: UtilService, private router:Router) { }

  ngOnInit() {
    this.utilService.blockUiStart()
    this.service.getAll('products').subscribe(data =>{ 
      this.utilService.blockUiStop();
      this.products = data 
      for(let p of this.products){
        p.cantSelect = 1;
        p.priceItemsTotal = p.pr_price;
      }
    });
    this.service.getAll("categories").subscribe(data =>{ this.categories = data} );
  }

  addProductList(product){
    this.listProduct.push(product);
  }

  removeProduct(id){
    let i = 0
    for(let p of this.listProduct){
      if(p.id == id){
        this.listProduct.splice(i, 1)
        break
      }
      i++;
    }
  }

  goPayment(){
    localStorage.setItem('listProduct', JSON.stringify(this.listProduct));
    this.router.navigate(['/cars-payment']);
  }

}
