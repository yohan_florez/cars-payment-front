import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsPaymentComponent } from './cars-payment.component';

describe('CarsPaymentComponent', () => {
  let component: CarsPaymentComponent;
  let fixture: ComponentFixture<CarsPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarsPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
