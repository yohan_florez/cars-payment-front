import { Component, OnInit } from '@angular/core';
import { Products } from '../../product';
import { ServiceModule } from 'src/app/services/service.module';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-cars-payment',
  templateUrl: './cars-payment.component.html',
  styleUrls: ['./cars-payment.component.scss']
})
export class CarsPaymentComponent implements OnInit {

  q:number = 1;
  listProduct:any = [];
  total = 0;
  good:boolean = false;

  constructor(private service: ServiceModule, private utilservice: UtilService) { }

  more(p){
    if( p.cantSelect < p.pr_quantity)
      p.cantSelect++;
      p.priceItemsTotal = (p.pr_price * p.cantSelect);
      this.totalResult();
  }

  less(p){
    if(p.cantSelect > 1)
      p.cantSelect--;
      p.priceItemsTotal = (p.pr_price * p.cantSelect);
      this.totalResult();
  }

  ngOnInit() {
    this.listProduct = JSON.parse(localStorage.getItem('listProduct'));
    this.totalResult();
  }

  totalResult(){
    this.total = 0;
    for(let p of this.listProduct)
      this.total += parseInt(p.priceItemsTotal);
  }

  payment(){
    this.utilservice.blockUiStart();
    this.service.saveOrUpdate('order', {or_total: this.total, products: this.listProduct  }).subscribe(data => {
      this.utilservice.blockUiStop();
      this.good = true;
      localStorage.clear();
      this.listProduct = [];
      setTimeout(() =>{this.good = false;}, 3000);
    })
  }

}
