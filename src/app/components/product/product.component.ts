import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @Input() product:any;
  @Output() productSelect = new EventEmitter();
  @Output() productDelete = new EventEmitter()

  q:number = 1;
  maxQDishes = 12;
  checked:boolean = false;

  constructor() { 
    
  }

  more(){
    if( this.q < this.product.pr_quantity)
      this.q++;
      this.product.priceItemsTotal = (this.product.pr_price * this.q);
      this.product.cantSelect = this.q;
  }

  less(){
    if(this.q > 1)
      this.q--;
      this.product.priceItemsTotal = (this.product.pr_price * this.q);
      this.product.cantSelect = this.q;
  }

  addProduct(){
    this.checked = true;
    this.productSelect.emit(this.product);
   }

   removeProduct(){
     this.checked = false;
     this.productDelete.emit(this.product.id);
   }

  ngOnInit() {
    
  }

}
